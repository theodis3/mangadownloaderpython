from manganode import MangaNode
from helpers import download_soup, parse_soup
from html.parser import HTMLParser

ROOT = "https://www.mangago.org"

class MangaList:
    def __iter__(self):
        soup = download_soup("https://www.mangago.org/manga-list")
        self.iter = iter(soup.select(".block-content a[title]"))
        return self

    def __next__(self):
        a = next(self.iter)
        return Manga(a.text.strip(), ROOT + a.attrs["href"])

class Manga(MangaNode):
    def __iter__(self):
        soup = download_soup(self.url)
        self.iter = iter(soup.select(".block-content tr"))
        next(self.iter)
        return self

    def __next__(self):
        tr = next(self.iter)
        a = tr.select("a")[0]
        name = a.text.strip()
        url = ROOT + a.attrs["href"]
        return MangaChapter(name, url)

class MangaChapter(MangaNode):
    def __iter__(self):
        soup = download_soup(self.url)
        result = soup.select(".img-responsive")
        self.iter = iter(result)
        self.count = 0
        return self

    def __next__(self):
        a = next(self.iter)
        self.count += 1
        return MangaPage(str(self.count), ROOT + a.attrs["src"])

class MangaPage(MangaNode):
    pass

def manga_list_iter():
    return MangaList()

