from manganode import MangaNode
from helpers import download_soup, parse_soup
from html.parser import HTMLParser

class MangaList:
    def __iter__(self):
        # Get index pages
        URL='https://www.funmanga.com/manga-list'
        soup = download_soup(URL)
        self.urls = []
        for tag in soup.select('.widget-heading a:not(.active)'):
            self.urls.append(tag.attrs["href"])

        self.cur_iter = iter(soup.select('.manga-list a'))
        return self

    def __next__(self):
        def get_next():
            a = next(self.cur_iter)
            return Manga(a.text.strip(), a.attrs["href"])
        try:
            return get_next()
        except StopIteration:
            if len(self.urls) == 0:
                raise StopIteration
            soup = download_soup(self.urls.pop(0))
            self.cur_iter = iter(soup.select('.manga-list a'))
            return get_next()

class Manga(MangaNode):
    def __iter__(self):
        soup = download_soup(self.url)
        self.iter = iter(reversed(soup.select(".chapter-list a")))
        return self

    def __next__(self):
        a = next(self.iter)
        name = a.find("span", {"class":"val"}).text.strip()
        url = a.attrs["href"] + "/all-pages"
        return MangaChapter(name, url)

class MangaChapter(MangaNode):
    def __iter__(self):
        soup = download_soup(self.url)
        self.iter = iter(soup.select(".img-responsive"))
        self.count = 0
        return self

    def __next__(self):
        a = next(self.iter)
        self.count += 1
        return MangaPage(str(self.count), "https:" + a.attrs["src"])

class MangaPage(MangaNode):
    pass

def manga_list_iter():
    return MangaList()

