from manganode import MangaNode
from helpers import download_soup, parse_soup
from html.parser import HTMLParser

class MangaList:
    def __iter__(self):
        self.page = 0
        self.page_max = 2
        self.cur_iter = iter([])
        return self

    def __next__(self):
        URL='https://h.mangairo.com/manga-list/type-latest/ctg-all/state-all/page-'
        def get_next():
            a = next(self.cur_iter)
            return Manga(a.text.strip(), a.attrs["href"])
        try:
            return get_next()
        except StopIteration:
            if self.page >= self.page_max:
                raise StopIteration
            self.page += 1
            soup = download_soup(URL + str(self.page))
            self.page_max = int(soup.select(".group-page a.go-p-end")[0].text[5:-1])
            self.cur_iter = iter(soup.select('.story-item h3 a'))
            return get_next()

class Manga(MangaNode):
    def __iter__(self):
        soup = download_soup(self.url)
        self.iter = iter(reversed(soup.select(".chapter_list a")))
        return self

    def __next__(self):
        a = next(self.iter)
        name = a.text.strip()
        url = a.attrs["href"]
        return MangaChapter(name, url)

class MangaChapter(MangaNode):
    def __iter__(self):
        soup = download_soup(self.url)
        self.iter = iter(soup.select(".panel-read-story .img_content"))
        self.count = 0
        return self

    def __next__(self):
        a = next(self.iter)
        self.count += 1
        return MangaPage(str(self.count), a.attrs["src"])

class MangaPage(MangaNode):
    pass

def manga_list_iter():
    return MangaList()

