from manganode import MangaNode
from helpers import download_soup, parse_soup
from html.parser import HTMLParser

ROOT="https://mangatoon.mobi"

class MangaList:
    def __iter__(self):
        self.next = "https://mangatoon.mobi/en/genre/comic?page=0"
        self.cur_iter = iter([])
        return self

    def __next__(self):
        def get_next():
            a = next(self.cur_iter)
            return Manga(a.find("div", {"class": "content-title"}).text.strip(), ROOT + a.attrs["href"] + "/episodes")
        try:
            return get_next()
        except StopIteration:
            if self.next is None:
                raise StopIteration
            soup = download_soup(self.next)
            next_tag = soup.select(".page a")[-1]
            if(next_tag.text.strip()[0:4]) == "Next":
                self.next = ROOT + next_tag.attrs["href"]
            else:
                self.next = None
            self.cur_iter = iter(soup.select('.items a'))
            return get_next()

class Manga(MangaNode):
    def __iter__(self):
        soup = download_soup(self.url)
        self.iter = iter(soup.select(".episode-item-new"))
        return self

    def __next__(self):
        a = next(self.iter)
        name = a.find("div", {"class": "episode-title-new"}).text.strip()
        url = ROOT + a.attrs["href"]
        return MangaChapter(name, url)

class MangaChapter(MangaNode):
    def __iter__(self):
        soup = download_soup(self.url)
        self.iter = iter(soup.select(".pictures img"))
        self.count = 0
        return self

    def __next__(self):
        a = next(self.iter)
        self.count += 1
        return MangaPage(str(self.count), a.attrs["src"])

class MangaPage(MangaNode):
    pass

def manga_list_iter():
    return MangaList()

