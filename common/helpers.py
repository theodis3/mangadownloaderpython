from bs4 import BeautifulSoup
import urllib
import os
import time

import tempfile, shutil
def create_temporary_copy(path):
    temp_dir = tempfile.gettempdir()
    temp_path = os.path.join(temp_dir, 'temp_file_name')
    shutil.copy2(path, temp_path)
    return temp_path

def download_page(url, retries=5):
    try:
        req = urllib.request.Request(
            url,
            data=None,
            headers={ 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36' }
        )
        return urllib.request.urlopen(req)
    except:
        if(retries > 0):
            time.sleep(1)
            return download_page(url, retries - 1)
        else:
            raise "Failed to download..."

def download_soup(url):
    return BeautifulSoup(download_page(url), 'html.parser')

def parse_soup(html):
    return BeautifulSoup(html, 'html.parser')

def get_files(directory, ext):
    return list(filter(lambda x: x[-len(ext):] == ext, os.listdir(directory)))

def get_file_string(filename):
    try:
        with open(filename, 'r', encoding="utf8") as file:
            ret = file.read()
        return ret
    except:
        with open(filename, 'r', encoding="ISO-8859-1") as file:
            ret = file.read()
        return ret

def download_modules():
    ret = {}
    python_files = get_files("downloaders",".py")
    module_names = map(lambda x: x[0:-3], python_files)
    for module_name in module_names:
        module = getattr(__import__("downloaders." + module_name), module_name)
        if hasattr(module, "manga_list_iter"):
            ret[module_name] = module
    return ret
