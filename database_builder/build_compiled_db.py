#!/usr/bin/env python3.10

from helpers import create_temporary_copy
from unidecode import unidecode
import sys
import sqlite3
import os
import re

def simple_normalize(name):
    return name.replace(" ","").replace("\t","").replace("\n","").upper()
def aggressive_normalize(name):
    return re.sub(r'\W+', '', unidecode(name).upper())

temp_db = create_temporary_copy(sys.argv[1])
conn = sqlite3.connect(temp_db)
c = conn.cursor()
# Drop old manga map
try:
    c.execute("""
        DROP TABLE manga_map
    """)
except:
    pass

# Create manga map with easy matches
c.execute("""
    CREATE TABLE manga_map AS SELECT series_id, downloader, url FROM manga_index_entries INNER JOIN titles ON manga_index_entries.name = titles.title GROUP BY downloader, url;
""")

# Get manga names
c.execute("""
    SELECT DISTINCT me.series_id, titles.title, bayesian_rating FROM series AS me INNER JOIN titles on me.series_id = titles.series_id
""")

name_rows = c.fetchall()

simple_name_map = {}
aggressive_name_map = {}
for name in name_rows:
    simple_name = simple_normalize(name[1])
    if  (not (simple_name in simple_name_map)) or \
        simple_name_map[simple_name]["rating"] < (name[2] or 0):
        simple_name_map[simple_name] = { "id": name[0], "rating": name[2] or 0 }
    aggressive_name = aggressive_normalize(name[1])
    if len(aggressive_name) > 3:
        if  (not (aggressive_name in aggressive_name_map)) or \
            aggressive_name_map[aggressive_name]["rating"] < (name[2] or 0):
            aggressive_name_map[aggressive_name] = { "id": name[0], "rating": name[2] or 0 }

# Get unmatched entries to try and match
c.execute("""
    SELECT manga_index_entries.downloader,name,manga_index_entries.url FROM manga_index_entries LEFT JOIN manga_map ON manga_index_entries.downloader = manga_map.downloader AND manga_index_entries.url = manga_map.url WHERE series_id IS null;
""")

unmatched_rows = c.fetchall()
for row in unmatched_rows:
    id = 0
    simple_name = simple_normalize(row[1])
    if simple_name in simple_name_map:
        id = simple_name_map[simple_name]["id"]
    if id == 0:
        aggressive_name = aggressive_normalize(row[1])
        if aggressive_name in aggressive_name_map:
            id = aggressive_name_map[aggressive_name]["id"]
    if id != 0:
        c.execute("INSERT INTO manga_map VALUES(?,?,?)", (id, row[0], row[2]))

# Clean out manga index table
c.execute("""
    DROP TABLE manga_index_entries
""")
c.execute("""
    DROP TABLE variables
""")
c.execute("""
    DROP TABLE manga_index
""")

# Clean out unmatched entries from manga update database
c.execute("""
    DELETE FROM series WHERE series_id NOT IN (SELECT series_id FROM manga_map)
""")
c.execute("""
    DELETE FROM titles WHERE series_id NOT IN (SELECT series_id FROM manga_map)
""")
c.execute("""
    DELETE FROM genres WHERE series_id NOT IN (SELECT series_id FROM manga_map)
""")
c.execute("""
    DELETE FROM categories WHERE series_id NOT IN (SELECT series_id FROM manga_map)
""")
c.execute("""
    DELETE FROM related WHERE series_id NOT IN (SELECT series_id FROM manga_map)
""")
c.execute("""
    DELETE FROM authors WHERE series_id NOT IN (SELECT series_id FROM manga_map)
""")
c.execute("""
    DELETE FROM publishers WHERE series_id NOT IN (SELECT series_id FROM manga_map)
""")
c.execute("""
    DELETE FROM publications WHERE series_id NOT IN (SELECT series_id FROM manga_map)
""")
c.execute("""
    DELETE FROM recommendations WHERE series_id NOT IN (SELECT series_id FROM manga_map)
""")
c.execute("""
    DELETE FROM category_recommendations WHERE series_id NOT IN (SELECT series_id FROM manga_map)
""")
c.execute("""
    DELETE FROM ranks WHERE series_id NOT IN (SELECT series_id FROM manga_map)
""")

conn.commit()
for line in conn.iterdump():
    print(line)
conn.close()
os.remove(temp_db)
