#!/usr/bin/env python3.10

from helpers import download_modules
import threading
import sqlite3

mangas = []

class DownloaderThread (threading.Thread):
    def __init__(self, downloader_name, downloader):
        threading.Thread.__init__(self)
        self.downloader_name = downloader_name
        self.downloader = downloader
        self.running = False
    def run(self):
        self.running = True
        manga_list_iter = getattr(self.downloader, "manga_list_iter")
        for manga in manga_list_iter():
            if not self.running:
                break
            manga_obj = (
                self.downloader_name,
                manga.name,
                manga.url
                )
            mangas.append(manga_obj)
        self.running = False
    def stop(self):
        self.running = False

threads = []
dms = download_modules()
for downloader_name in dms:
    downloader = dms[downloader_name]
    threads.append(DownloaderThread(downloader_name, downloader))

for thread in threads:
    thread.start()
for thread in threads:
    thread.join()

conn = sqlite3.connect(":memory:")
c = conn.cursor()
c.execute("""
    CREATE TABLE manga_index_entries(
        downloader,
        name,
        url)
""")
c.executemany("INSERT INTO manga_index_entries VALUES(?,?,?)", mangas)
conn.commit()
for line in conn.iterdump():
    print(line)
conn.close()
