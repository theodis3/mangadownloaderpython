#!/usr/bin/env python3

from helpers import create_temporary_copy, parse_soup
import sys
import sqlite3
import tarfile
import tempfile
import os
import re

# Average: 6.9 / 10.0 (9 votes)Bayesian Average: 6.71 / 10.0
rating_re = re.compile("Average: (1?[0-9](\.[0-9]+)?) / 10.0 \(([1-9][0-9]*) votes\)Bayesian Average: (1?[0-9](\.[0-9]+)?) / 10.0.*")

# id, type, year, description, status, completely_scanlated, licensed, average_rating, bayesian_rating, votes, image_url, last_updated
manga_update_entries = []

# id, name
names = []

# id, id_related, relation
related = []

# id, genre
genres = []

# id, category
categories = []

conn = sqlite3.connect(":memory:")

cur = conn.cursor()
# id, name, type, year, description, status, completely_scanlated, licensed, average_rating, image_url, last_updated
cur.execute("""
    CREATE TABLE manga_update_entries(
        id,
        name,
        type,
        year,
        description,
        status,
        completely_scanlated,
        licensed,
        average_rating,
        bayesian_rating,
        votes,
        image_url,
        last_updated
    )
""")
# id, name
cur.execute("""
    CREATE TABLE manga_update_names(
        id,
        name
    )
""")
# id, id_related, relation
cur.execute("""
    CREATE TABLE manga_update_related(
        id,
        id_related,
        relation
    )
""")
# id, genre
cur.execute("""
    CREATE TABLE manga_update_genres(
        id,
        genre
    )
""")
# id, category
cur.execute("""
    CREATE TABLE manga_update_categories(
        id,
        category
    )
""")

def replace_brs(string):
    return string.replace("<br>","\n").replace("</br>","\n").replace("<br/>","\n")
def split_brs(string):
    name_strs = replace_brs("".join(map(str, string))).split("\n")
    name_strs = map(lambda x: x.strip(), name_strs)
    name_strs = filter(lambda x: len(x) > 0, name_strs)
    name_strs = list(name_strs)
    return name_strs

def get_data(filestring, id):
    if "You specified an invalid series id." in filestring:
        return
    start_string = "<!-- Start:Series Info-->"
    end_string = "<!-- End:Series Info-->"

    try:
        start_ind = filestring.index(start_string)
    except:
        return

    start_ind += len(start_string)
    end_ind = filestring.index(end_string, start_ind)
    filestring = filestring[start_ind:end_ind]

    soup = parse_soup(filestring)

    category_text = list(map(lambda tag: tag.text.strip(), soup.select(".sCat")))
    content_nodes = soup.select(".sContent")
    data = {}
    for i in range(0, len(category_text)):
        data[category_text[i]] = content_nodes[i]
    obj = {
        "id": int(id),
        "Description": None,
        "Type": None,
        "Year": None,
        "Status in Country of Origin": None,
        "Completely Scanlated?": None,
        "Last Updated": None,
        "Licensed (in English)": None,
        "User Rating": None,
        "Image [Report Inappropriate Content]": None,
        "Associated Names": [],
        "Related Series": [],
        "Genre": [],
        "Categories": []
        }
    obj["Associated Names"].append(soup.select(".releasestitle")[0].text.strip())
    for category in data:
        if  data[category].text.strip()[0:3] == "N/A":
            continue
        if  category == "Description" or \
            category == "Type" or \
            category == "Year" or \
            category == "Status in Country of Origin" or \
            category == "Completely Scanlated?" or \
            category == "Last Updated" or \
            category == "Licensed (in English)":
            obj[category] = data[category].text.strip()
        elif category == "Associated Names":
            obj[category] += split_brs(data[category].contents)
        elif category == "Related Series":
            related_strs = split_brs(data[category].contents)
            related_items = []
            for related_str in related_strs:
                psoup = parse_soup(related_str)
                a = psoup.select("a")[0]
                id_str = "series.html?id="
                id = int(a.attrs["href"][len(id_str):])
                type = a.next_sibling.strip()[1:-1]
                related_items.append({"id": id, "type": type})
            obj[category] = related_items
        elif category == "Genre":
            obj[category] = list(map(lambda tag: tag.text.strip(), data[category].select("a")[:-1]))
        elif category == "Categories":
            obj[category] = list(map(lambda tag: tag.text.strip(), data[category].select("li a[title]")))
        elif category == "User Rating":
            results = rating_re.search(data[category].text)
            if(len(results.groups()) == 5):
                average_rating = results.group(1) # Average
                votes = results.group(3) # Votes
                bayesian_rating = results.group(4) # Bayesian

                obj[category] = {
                    "average_rating": average_rating,
                    "bayesian_rating": bayesian_rating,
                    "votes": votes
                }
        elif category == "Image [Report Inappropriate Content]":
            imgs = data[category].select("img[src]")
            if(len(imgs) > 0):
                obj[category] = imgs[0].attrs["src"].strip()

    manga_update_entries.append((
        obj["id"],
        obj["Associated Names"][0],
        obj["Type"],
        obj["Year"],
        obj["Description"],
        obj["Status in Country of Origin"],
        obj["Completely Scanlated?"],
        obj["Licensed (in English)"],
        None if obj["User Rating"] is None else float(obj["User Rating"]["average_rating"]),
        None if obj["User Rating"] is None else float(obj["User Rating"]["bayesian_rating"]),
        None if obj["User Rating"] is None else int(obj["User Rating"]["votes"]),
        obj["Image [Report Inappropriate Content]"],
        obj["Last Updated"]))
    for name in list(dict.fromkeys(obj["Associated Names"])):
        names.append((obj["id"], name))
    for series in obj["Related Series"]:
        related.append((obj["id"], series["id"], series["type"]))
    for genre in obj["Genre"]:
        genres.append((obj["id"], genre))
    for category in obj["Categories"]:
        categories.append((obj["id"], category))

def send_entries_to_db():
    cur.executemany("INSERT INTO manga_update_entries VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)", manga_update_entries)
    cur.executemany("INSERT INTO manga_update_names VALUES(?,?)", names)
    cur.executemany("INSERT INTO manga_update_related VALUES(?,?,?)", related)
    cur.executemany("INSERT INTO manga_update_genres VALUES(?,?)", genres)
    cur.executemany("INSERT INTO manga_update_categories VALUES(?,?)", categories)

    manga_update_entries.clear()
    names.clear()
    related.clear()
    genres.clear()
    categories.clear()

temp_pages = create_temporary_copy(sys.argv[1])
file = tarfile.open(temp_pages)

for info in file:
    fbuf = file.extractfile(info)
    if(fbuf is not None):
        id = info.name[6:-5]
        filestring = fbuf.read().decode("utf-8", "replace")
        get_data(filestring, id)
        if(len(manga_update_entries) >= 1000):
            send_entries_to_db()

send_entries_to_db()
conn.commit()
for line in conn.iterdump():
    print(line)
conn.close()
os.remove(temp_pages)
