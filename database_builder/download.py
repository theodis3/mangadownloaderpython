#!/usr/bin/env python3

import urllib.request
import time
import sys

def download_page(url, delay=1):
    try:
        req = urllib.request.Request(
            url,
            data=None,
            headers={ 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36' }
        )
        with urllib.request.urlopen(req) as response:
            html_response = response.read()
            encoding = response.headers.get_content_charset('utf-8')
            decoded_html = html_response.decode(encoding)
            return decoded_html
    except:
        time.sleep(delay)
        return download_page(url, min(delay * 2, 3600))


print(download_page(sys.argv[1]))
