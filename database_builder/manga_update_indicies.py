#!/usr/bin/env python3

from helpers import download_soup

next_page = "https://www.mangaupdates.com/series.html?page=1&display=list&perpage=100&orderby=year"
while next_page is not None:
    soup = download_soup(next_page)
    manga_a = soup.select("#main_content .text a[href][alt]")
    for a in manga_a:
        url = a.attrs["href"]
        id_pos = url.index("?id=")
        print(url[id_pos+4:])
    next_page_a = soup.select(".justify-content-end.specialtext a")
    if(len(next_page_a) == 1):
        next_page = next_page_a[0].attrs["href"]
    else:
        next_page = None
