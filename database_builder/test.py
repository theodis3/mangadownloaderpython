#!/usr/bin/env python3

import tarfile

# open file
file = tarfile.open('pages.tar.gz')

for info in file:
    fbuf = file.extractfile(info)
    if(fbuf is not None):
        print(info.name)
        print(fbuf.read().decode("utf-8", "replace"))
