import gi
import sqlite3
import unidecode

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class MangaTreeStore(Gtk.TreeStore):
    def __init__(self, conn):
        super(MangaTreeStore,self).__init__(str, int)

        def firstLetterOrNumber(string):
            ret = string
            for i in range(len(string)):
                letter = string[i]
                if( (letter >= 'A' and letter <= 'Z') or
                    (letter >= 'a' and letter <= 'z') or
                    (letter >= '0' and letter <= '9')):
                    ret = string[i:]
                    break
                dletter = unidecode.unidecode(letter)
                if( (dletter >= 'A' and dletter <= 'Z') or
                    (dletter >= 'a' and dletter <= 'z') or
                    (dletter >= '0' and dletter <= '9')):
                    ret = string[i:]
                    break
            return ret

        cur = conn.cursor()

        cur.execute("SELECT name,id FROM manga_update_entries");
        mm = {}
        rows = cur.fetchall()
        for row in rows:
            letter = unidecode.unidecode(firstLetterOrNumber(row[0])[0].upper())

            if(letter >= 'A' and letter <= 'Z'):
                pass
            elif(letter >= '0' and letter <= '9'):
                letter = "0"
            else:
                letter = "*"
            if not letter in mm:
                mm[letter] = []
            mm[letter].append(row)
        keys = list(mm.keys())
        keys.sort()

        for key in keys:
            mm[key].sort(key=lambda s: unidecode.unidecode(firstLetterOrNumber(s[0])).lower())
            node = self.append(None, [key, 0])
            for row in mm[key]:
                self.append(node, row)

class MangaTree(Gtk.TreeView):
    def __init__(self, conn, change_callback):
        self.store = MangaTreeStore(conn)
        super(MangaTree,self).__init__(model=self.store)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Mangas", renderer, text=0)
        self.append_column(column)

        def onSelectionChanged(selection):
            (model, pathlist) = selection.get_selected_rows()
            for path in pathlist:
                tree_iter = model.get_iter(path)
                self.selected_id = model.get_value(tree_iter,1)
            if(self.selected_id > 0):
                change_callback(self.selected_id)

        self.tree_selection = self.get_selection()
        self.tree_selection.connect("changed", onSelectionChanged)

class MangaTreeWindow(Gtk.ScrolledWindow):
    def __init__(self, conn):
        super(MangaTreeWindow,self).__init__()
        def selection_changed(id):
            for callback in self.changed_callbacks:
                callback(id)
        self.treeview = MangaTree(conn, selection_changed)
        self.set_vexpand(True)
        self.add(self.treeview)

        self.changed_callbacks = []
    def add_changed_callback(self, callback):
        self.changed_callbacks.append(callback)
