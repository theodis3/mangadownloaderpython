import gi
import urllib
import urllib.request

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gio
from gi.repository.GdkPixbuf import Pixbuf

class WebImage(Gtk.Image):
    def __init__(self):
        super(WebImage,self).__init__()
    def load_url(self, url):
        req = urllib.request.Request(
            url,
            data=None,
            headers={ 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36' }
        )
        response = urllib.request.urlopen(req)
        input_stream = Gio.MemoryInputStream.new_from_data(response.read(), None)
        self.set_from_pixbuf(Pixbuf.new_from_stream(input_stream))

