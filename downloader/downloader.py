#!/usr/bin/env python3

import gi
import sqlite3

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

conn = sqlite3.connect("compiled.db")
cur = conn.cursor()

class MyWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Hello World")

        pane = Gtk.Paned()

        self.tree = MangaTree()

        self.add(pane)
        pane.add1(self.tree)


class MangaTree(Gtk.TreeView):
    def __init__(self):

        pass

win = MyWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
