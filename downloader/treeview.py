#!/usr/bin/env python3
import gi
import sqlite3

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

from components.mangatree import MangaTreeWindow
from components.webimage import WebImage

class TreeViewFilterWindow(Gtk.Window):
    def __init__(self):
        def id_changed(id):
            cur = conn.cursor()

            cur.execute("SELECT * FROM manga_update_entries WHERE id = ?", (id,));
            rows = cur.fetchall()
            print(rows)
        Gtk.Window.__init__(self, title="Treeview Filter Demo")
        self.set_border_width(10)
        #self.manga_tree = MangaTreeWindow(conn)
        #self.manga_tree.add_changed_callback(id_changed)
        #self.add(self.manga_tree)
        self.image = WebImage()
        self.image.load_url("https://www.mangaupdates.com/image/i293154.png")
        self.add(self.image)
        self.show_all()

conn = sqlite3.connect("manga.db")
win = TreeViewFilterWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
conn.close()
