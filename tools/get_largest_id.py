#!/usr/bin/env python3

import os
import re
import sys

pattern = re.compile(".*href='https://www.mangaupdates.com/series.html\?id=(\d+)'.*")

max = 0
for line in sys.stdin:
    match = pattern.match(line)
    if match is not None:
        id = int(match.group(1))
        if(id > max):
            max = id
print(max)
